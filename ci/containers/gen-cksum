#!/bin/bash

# Generate the image checksum for an image generated with the given files.
# Usage: gen-cksum <image>:<arch>

image="$(echo "$1" | cut -d: -f1)" || exit $?
# arch="$(echo "$1" | cut -d: -f2)" || exit $?

trap 'rm -f "$tmp"' EXIT
tmp="$(mktemp)" || exit $?

hash_rename() {
  shahash="$(sha512sum "$1")" || return $?
  shahash="$(echo "$shahash" | cut -d' ' -f1)" || return $?
  printf "%s\t%s\n" "$shahash" "$2" || return $?
}

# Hash the Containerfile and any files that it references in order
parse_containerfile() {
  hash_rename "$1" "$2$(basename "$1")" >> "$tmp" || exit $?

  local prefix
  prefix="$(dirname "$1")" || return $?
  # shellcheck disable=SC2162
  while read command line; do
    if [ "$command" = COPY ] || [ "$command" = ADD ]; then
      for word in $line; do
        if ! [[ "$word" =~ :// ]] && ! [[ "$word" =~ ^/ ]] \
           && [ -e "$CI_PROJECT_DIR"/ci/containers/"$image"/"$word" ]; then
          hash_rename "$CI_PROJECT_DIR"/ci/containers/"$image"/"$word" "$word" >> "$tmp" || return $?
        fi
      done
    elif [ "$command" = INCLUDE ]; then
      parse_containerfile "$prefix"/"$line" "$2$(dirname "$line")/" || return $?
    fi
  done < "$1"
}
parse_containerfile "$CI_PROJECT_DIR"/ci/containers/"$image"/Containerfile.in || exit $?

# Hash the installed Spack environments, possibly with concrete forms
hash_rename "$CI_PROJECT_DIR"/ci/dependencies/latest/spack.yaml \
  /spack/cenv/latest/spack.yaml >> "$tmp" || exit $?
hash_rename "$CI_PROJECT_DIR"/ci/dependencies/minimum/spack.yaml \
  /spack/cenv/minimum/spack.yaml >> "$tmp" || exit $?

# Checksum the checksum file to generate the final checksum
echo -n 'sha256:'
sha256sum "$tmp" | cut -d' ' -f1
