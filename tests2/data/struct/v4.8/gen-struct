#!/usr/bin/env python3

import shutil
import subprocess
import typing
from pathlib import Path

import click
from hpctoolkit.test.execution import hpcstruct

NVIDIA_MAGIC = b"\x7fELF\x02\x01\x01\x33\x07\0\0\0\0\0\0\0\x02\0\xbe\0"


def check_gpucfg_supported(binary: Path):
    """Check if we expect --gpucfg yes to work for the given binary.
    Raises a ClickException if anything looks like it won't work.
    """
    # Read a prefix from the binary to assist with identification
    with open(binary, "rb") as f:
        prefix = f.read(20)

    if prefix.startswith(NVIDIA_MAGIC):
        # hpcstruct will be using nvdisasm from the PATH to parse this binary.
        # Run nvdisasm ourselves and determine if it will work.
        nvdisasm = shutil.which("nvdisasm")
        if nvdisasm is None:
            raise click.ClickException("nvdisasm not found, --gpucfg yes will not work!")
        proc = subprocess.run([nvdisasm, binary], stdout=subprocess.DEVNULL, check=False)
        if proc.returncode != 0:
            raise click.ClickException(
                "nvdisasm failed to parse binary, so --gpucfg yes will not work"
            )


@click.command()
@click.argument("binary", type=click.Path(exists=True, dir_okay=False, path_type=Path))
@click.argument("output", type=click.File("wb"))
@click.option("--gpucfg/--no-gpucfg", default=False)
def gen_struct(binary: Path, output: typing.BinaryIO, gpucfg: bool):
    """Analyze the BINARY and generate an OUTPUT Structfile, running `hpcstruct ARGS...`."""
    if gpucfg:
        check_gpucfg_supported(binary)

    with hpcstruct(binary, "--gpucfg", "yes" if gpucfg else "no", threads=1) as structfile:
        shutil.copyfileobj(structfile, output)


if __name__ == "__main__":
    gen_struct()  # pylint: disable=no-value-for-parameter
